# REVOLUT TEST TASK

* RESTful app implemented using spark-java web-micro-framework
* jUnit-based tests included
# Internals
## architecture
Internal app architecture is feature-based instead of more common layer-based.
It's sort of preferred for spark and what's more important preferred for small projects
## build test and run
to build and test execute maven goals: test/package.
app is packaged into single jar: rest-transaction-0.9.5-full.jar, Main class included into manifest, so running is simple:
`java -jar rest-transaction-0.9.5-full.jar`
# API
All interactions are JSON or empty body http requests
Supported REST methods:

| Method             | Descr   | request body | response |
|--------------------|---------|--------------|----------|
| get /account/      | Get account list  | `""`  | `[{"name":*acc_name*, "balance":*balance*},...]`  |
| put /account/      | Create new account | `{"name":"new_name"}` | `{"name":"new_name", "balance":0.0}` |
| get /account/:name | Get current balance for account with given name | `""` | `{"name":"name", "balance":0.0}` |
| put /transfers/    | Create transfer request from one account to another | `{"from":"from","to":"to", "amount":10}` | `{"source":{"name":"from","balance":-10.0},"target":{"name":"to","balance":10.0},"amount":10.0}`         |

# Notes:
* for simplicity's sake there's no option to delete accounts (I guess that is well-aligned with real-world complience policies)
* Any account can go infinitely into debt, *we are generous creditors* =)
* There are no checks for balance overflow, *I just have that in mind* (It can be easily added by tweaking Account internals)
* There are lots of java-docs missing since I think that java code is pretty self-documenting and project is really small.

