package app.transfer;

/***
 * this was supposed to be package-private,
 * switched for testing needs, I'm probably just bad at jUnit
 */
public class TransferRequest {

    private String from;
    private String to;
    private double amount;

    public TransferRequest(){}

    public TransferRequest(String from, String to, double amount){
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    String getFrom() {
        return from;
    }

    void setFrom(String from) {
        this.from = from;
    }

    String getTo() {
        return to;
    }

    void setTo(String to) {
        this.to = to;
    }

    double getAmount() {
        return amount;
    }

    void setAmount(double amount) {
        this.amount = amount;
    }
}
