package app.transfer;

import app.account.Account;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class TransferDAO {

    private ConcurrentHashMap<Long, Transfer> committed;
    private ConcurrentHashMap<Long, Transfer> created;
    private AtomicLong generatedId;

    public TransferDAO() {
        this.committed = new ConcurrentHashMap<>();
        this.created = new ConcurrentHashMap<>();
        generatedId = new AtomicLong();
    }

    public TransferDAO(int capacity, float loadFactor, int concurrencyLevel) {
        this.committed = new ConcurrentHashMap<>(capacity, loadFactor, concurrencyLevel);
        this.created = new ConcurrentHashMap<>(capacity, loadFactor, concurrencyLevel);
        generatedId = new AtomicLong();
    }

    Long createTransfer(Account from, Account to, double amount) {
        Transfer transfer = new Transfer(from, to, amount);
        Long reservedId = generatedId.incrementAndGet();
        created.put(reservedId, transfer);
        return reservedId;
    }

    /***
     * This is sort-of idea of simulation of transaction behavior
     * was left unfinished since it's useless in this app
     * Should have thrown transactionAbortedException or something like that to rollback account balances
     * @param id created transfer id
     * @return Committed transfer
     */
    Transfer commitTransfer(Long id) {
        Transfer transfer = created.remove(id);
        committed.put(id, transfer);
        return transfer;
    }

}
