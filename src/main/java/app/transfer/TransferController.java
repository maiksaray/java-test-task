package app.transfer;

import app.account.Account;
import app.account.AccountController;
import app.account.AccountDAO;
import app.account.OperationType;
import app.common.DaoException;
import app.common.EntityNotFoundException;
import app.common.ErrorResponse;
import app.util.ParameterLock;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.concurrent.locks.Lock;

public class TransferController {

    private static final Logger logger = LoggerFactory.getLogger(TransferController.class);

    private TransferDAO transferDAO;
    private AccountDAO accountDAO;
    private Gson gson;


    public TransferController(TransferDAO transferDAO, AccountDAO accountDAO) {
        this.transferDAO = transferDAO;
        this.accountDAO = accountDAO;
        gson = new Gson();
    }

    public Route processTransfer = (Request request, Response response) -> {
        try {
            String body = request.body();
            logger.info(String.format("Got request: transfer, params: %s", body));
            TransferRequest transferRequest = gson.fromJson(body, TransferRequest.class);
            if (transferRequest.getFrom().equals(transferRequest.getTo())) {
                response.status(400);
                logger.warn("request to transfer inside same account! rejecting");
                return gson.toJson(new ErrorResponse("Can't process transfer when source and target are the same"));
            }
            Transfer transfer = doProcessTrasfer(transferRequest);
            String json = gson.toJson(transfer);
            logger.debug(String.format("successfully transfered! returning json: %s", json));
            return json;
        } catch (JsonParseException e) {
            response.status(400);
            logger.warn("could not parse transfer request !");
            return gson.toJson(new ErrorResponse("Invalid request"));
        } catch (EntityNotFoundException e) {
            response.status(404);
            logger.warn(String.format("Account with name %s could not be found, transfer aborted", e.getName()));
            return gson.toJson(new ErrorResponse("No account found: " + e.getMessage()));
        }
    };

    /***
     * Service-like internal method, there's no need to put it into separate class in such a small app
     * @param transferRequest request object
     * @return committed transfer
     * @throws DaoException
     */
    private Transfer doProcessTrasfer(TransferRequest transferRequest) throws EntityNotFoundException {

        // There's no option to delete accounts and there's no need to keep track/lock after we successfully get
        logger.debug("obtaining accounts with names: %s, %s", transferRequest.getFrom(), transferRequest.getTo());
        Account from = accountDAO.getAccount(transferRequest.getFrom());
        Account to = accountDAO.getAccount(transferRequest.getTo());
        logger.debug("Successfully obtained accounts");
        double amount = transferRequest.getAmount();
        Long id = transferDAO.createTransfer(from, to, amount);
        logger.debug("creating transfer");

        logger.debug("Obtaining Locks for accounts");
        // Account-specific locks are used here so that transfers are synchronized only for same Accounts
        // This looks like an elegant solution to the sync problem
        // However, I had ideas about some lock-free algorithms based on CAS, but decided not to implement those
        Lock lockFrom = ParameterLock.getCanonicalParameterLock(from);
        Lock lockTo = ParameterLock.getCanonicalParameterLock(to);

        // Avoiding possible deadlock for two simultaneous transfers from->to|to->from
        // By ordering locking we make sure that we don't lock by one resource in each thread
        if (from.getName().compareTo(to.getName()) > 0) {
            lockFrom.lock();
            lockTo.lock();
        } else {
            lockTo.lock();
            lockFrom.lock();
        }

        logger.debug("Locked on both accounts");
        accountDAO.updateBalance(from, amount, OperationType.SUBTRACT);
        accountDAO.updateBalance(to, amount, OperationType.ADD);

        logger.debug("unlocking");
        lockFrom.unlock();
        lockTo.unlock();

        logger.debug("successfully updated balances and unlocked accounts, commiting transfer");
        return transferDAO.commitTransfer(id);
    }

}
