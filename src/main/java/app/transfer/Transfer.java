package app.transfer;

import app.account.Account;

public class Transfer {

    private Account source;
    private Account target;
    private double amount;

    Transfer(Account source, Account target, double amount) {
        this.source = source;
        this.target = target;
        this.amount = amount;
    }

    public double getResultSourceBalance(){
        return this.source.getBalance();
    }

    public double getResultTargetBalance(){
        return this.target.getBalance();
    }
}
