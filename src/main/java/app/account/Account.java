package app.account;

public class Account {

    private String name;
    private double balance;

    Account(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Account))return false;
        Account otherAccount = (Account)other;
        return this.name.equals(otherAccount.getName());
    }
}
