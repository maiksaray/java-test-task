package app.account;

import app.common.EntityExistsException;
import app.common.EntityNotFoundException;
import app.common.ErrorResponse;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private Gson gson;

    private AccountDAO dao;

    public AccountController(AccountDAO dao) {
        this.dao = dao;
        this.gson = new Gson();
    }

    public Route getAccountBalance = (Request request, Response response) -> {
        String name = request.params("name");
        logger.info(String.format("Got request: getAccBalance, name %s", name));
        try {
            String json = gson.toJson(dao.getAccount(name));
            logger.debug(String.format("returning json %s", json));
            return json;
        } catch (EntityNotFoundException e) {
            logger.warn(String.format("Account with name %s NotFound!", e.getName()));
            response.status(404);
            return gson.toJson(new ErrorResponse("No account found: " + e.getMessage()));
        }
    };

    public Route createAccount = (Request request, Response response) -> {
        try {
            String body = request.body();
            logger.info(String.format("Got request: creteAccount, name %s", body));
            CreateAccountRequest createRequest = gson.fromJson(body, CreateAccountRequest.class);
            Account account = dao.createAccount(createRequest.getName());
            String json = gson.toJson(account);
            logger.debug(String.format("successfully created account, returning json: %s", json));
            return json;
        } catch (JsonParseException e) {
            response.status(400);
            logger.warn("could not parse account creation request !");
            return gson.toJson(new ErrorResponse("Invalid request"));
        } catch (EntityExistsException e) {
            response.status(409);
            logger.warn(String.format("Account with name %s already exists!", e.getName()));
            return gson.toJson(new ErrorResponse("Duplicate account name: " + e.getMessage()));
        }
    };

    public Route getAccountList = (Request request, Response response) -> gson.toJson(dao.getAccountList());

}
