package app.account;

public class CreateAccountRequest {

    private String name;

    public CreateAccountRequest(){}

    public CreateAccountRequest(String name){
        this.name = name;
    }

    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
