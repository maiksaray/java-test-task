package app.account;

import app.common.DaoException;
import app.common.EntityExistsException;
import app.common.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

public class AccountDAO {

    private ConcurrentHashMap<Long, Account> storage;
    private AtomicLong generatedId;
    private int concurrencyLevel;

    /***
     * Parametrized constructor for account DAO, basically all params are for internal concurrentHashmap storage
     * @param capacity base capacity
     * @param loadFactor load factor to extend capacity upon reaching
     * @param concurrencyLevel number of internal buckets
     */
    public AccountDAO(int capacity, float loadFactor, int concurrencyLevel) {
        this.concurrencyLevel = concurrencyLevel;
        generatedId = new AtomicLong();
        storage = new ConcurrentHashMap<>(capacity, loadFactor, this.concurrencyLevel);
    }

    public AccountDAO() {
        storage = new ConcurrentHashMap<>();
        generatedId = new AtomicLong();
//        Default concurrentHashMap concurrencyLevel
        concurrencyLevel = 16;
    }

    public Account createAccount(String name) throws EntityExistsException {
        Account account = new Account(name);
        if (storage.containsValue(account))
            throw new EntityExistsException(String.format("Attempt to create account with name %s failed, it already exists!", name), name);
        Long reservedId = generatedId.incrementAndGet();
        storage.put(reservedId, account);
        return account;
    }

    @Deprecated
    public Account getAccount(Long id) throws EntityNotFoundException {
        if(storage.containsKey(id))
            return storage.get(id);
        else
            throw new EntityNotFoundException(String.format("Account with id %d not found", id), "");
    }

    public Account getAccount(String name) throws EntityNotFoundException {
        Account account = storage.searchValues(this.concurrencyLevel,
                a -> a.getName().equals(name)
                        ? a
                        : null);
        if (account == null)
            throw new EntityNotFoundException(String.format("Account with name %s not found", name), name);
        return account;
    }

    List<Account> getAccountList() {
        return new ArrayList<>(storage.values());
    }

    public void updateBalance(Account account, double amount, OperationType type) {
        switch (type) {
            case ADD:
                account.setBalance(account.getBalance() + amount);
                break;
            case SUBTRACT:
                account.setBalance(account.getBalance() - amount);
                break;
        }
    }
}
