package app;

import app.account.AccountController;
import app.account.AccountDAO;
import app.transfer.TransferController;
import app.transfer.TransferDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static spark.Spark.*;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);


    public static void Run() {
        logger.info("application starting...");
        // Some default values
        int capacity = 100;
        float loadFactor = 0.75f;
        int concurrencyLevel = 16;

        String resourceName = "app.properties"; // could also be a constant
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties props = new Properties();
        try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
            props.load(resourceStream);
            capacity = Integer.parseInt(props.getProperty("capacity"));
            loadFactor = Float.parseFloat(props.getProperty("loadFactor"));
            concurrencyLevel = Integer.parseInt(props.getProperty("concurrencyLevel"));
            logger.info("properties read from resources");
        } catch (IOException e) {
            logger.warn("failed to read properties from resource file");
        } catch (NumberFormatException e) {
            logger.warn("incorrect resourse file formatting");
        }

        AccountDAO accountDAO = new AccountDAO(capacity, loadFactor, concurrencyLevel);
        TransferDAO transferDAO = new TransferDAO(capacity, loadFactor, concurrencyLevel);

        AccountController accountController = new AccountController(accountDAO);

        TransferController transferController = new TransferController(transferDAO, accountDAO);

        logger.info("App configured, igniting spark...");

        port(4567);

        get("/account/",        accountController.getAccountList);
        put("/account/",        accountController.createAccount);
        get("/account/:name",   accountController.getAccountBalance);
//        This is not really straight-forward approach to REST here
//        For the sake of simplicity of this app we(I really) don't allow editing account info
//        So POST request here stands for altering single inner account property: balance
        put("/transfer/",       transferController.processTransfer);
    }
}
