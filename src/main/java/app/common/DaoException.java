package app.common;

public class DaoException extends Exception {

    private String details;

    DaoException(String details){
        this.details = details;
    }

    @Override
    public String getMessage() {
//        It'd be nice to use stringBuilder for string concat, but cmon, it's exception text concat
        return details + super.getMessage();
    }
}
