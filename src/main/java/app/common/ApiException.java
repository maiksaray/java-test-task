package app.common;

public class ApiException extends Exception {

    private String details;

    public ApiException(String details) {

        this.details = details;
    }

    @Override
    public String getMessage() {
//        It'd be nice to use stringBuilder for string concat, but cmon, it's exception text concat
        return details + super.getMessage();
    }
}
