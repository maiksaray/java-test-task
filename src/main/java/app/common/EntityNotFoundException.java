package app.common;

public class EntityNotFoundException extends DaoException {

    private final String name;

    public EntityNotFoundException(String details, String name) {
        super(details);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
