package app.common;

public class EntityExistsException extends DaoException {

    private final String name;

    public EntityExistsException(String details, String name) {
        super(details);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
