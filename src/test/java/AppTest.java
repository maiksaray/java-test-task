
import app.Application;
import app.account.Account;
import app.account.CreateAccountRequest;
import app.common.ErrorResponse;
import app.transfer.Transfer;
import app.transfer.TransferRequest;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static spark.Spark.stop;

/**
 * The test class
 *
 * Ok, I know that jUnit is not meant for this, but c'mon. plus given the size of the app it's natural to keep
 * tooling as small as possible
 *
 */
class AppTest {

    private Gson gson = new Gson();
    private String firstAccountName = "first";

    private HttpUriRequest doCreateAccountRequest(String name) throws IOException {
        CreateAccountRequest createRequest = new CreateAccountRequest(name);
        StringEntity body = new StringEntity(gson.toJson(createRequest));
        HttpPut request = new HttpPut("http://localhost:4567/account/");
        request.setEntity(body);
        return request;
    }

    /***
     * Ok, I know it's bad to restart the server each time, but there's no way to clean-up inside the blackbox
     * and we do not want to have ordered tests.
     */
    @BeforeEach
    void startCleanServer() {
        Application.Run();
    }

    @AfterEach
    void stopServer(){
        stop();
    }

    @Test
    void testAccountCreation() throws IOException {

        // Given
        HttpUriRequest request = doCreateAccountRequest(firstAccountName);

        //When
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String json = EntityUtils.toString(response.getEntity());
        Account account = gson.fromJson(json, Account.class);

        // Then
        assertNotNull(account);
        assertEquals(account.getName(), firstAccountName);
        assertEquals(account.getBalance(), 0);
    }

//todo?: add some tests with invalid request data?

    @Test
    void testDuplicateName() throws IOException {

        // Given
        HttpUriRequest request1 = doCreateAccountRequest(firstAccountName);
        HttpUriRequest request2 = doCreateAccountRequest(firstAccountName);

        //When
        HttpResponse response1 = HttpClientBuilder.create().build().execute(request1);
        HttpResponse response2 = HttpClientBuilder.create().build().execute(request2);
        String json1 = EntityUtils.toString(response1.getEntity());
        String json2 = EntityUtils.toString(response2.getEntity());
        Account account = gson.fromJson(json1, Account.class);
        ErrorResponse errorResponse = gson.fromJson(json2, ErrorResponse.class);

        // Then
        assertNotNull(account);
        assertEquals(account.getName(), firstAccountName);
        assertEquals(account.getBalance(), 0);
        assertEquals(response2.getStatusLine().getStatusCode(), 409);
        assertTrue(errorResponse.getMessage().contains("already exists"));
    }

    @Test
    void testAccountBalance() throws IOException {

        // Given
        HttpClientBuilder.create().build().execute(doCreateAccountRequest(firstAccountName));
        HttpUriRequest request = new HttpGet("http://localhost:4567/account/" + firstAccountName);

        // When
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String json = EntityUtils.toString(response.getEntity());
        Account account = gson.fromJson(json, Account.class);

        // Then
        assertNotNull(account);
        assertEquals(account.getName(), firstAccountName);
        assertEquals(account.getBalance(), 0);
    }

    @Test
    void testSingleTransfer() throws IOException {
        // Given

        String fromName = "from";
        String toName = "to";
        int transferAmount = 10;

        HttpClientBuilder.create().build().execute(doCreateAccountRequest(fromName));
        HttpClientBuilder.create().build().execute(doCreateAccountRequest(toName));

        TransferRequest transferRequest = new TransferRequest(fromName, toName, transferAmount);
        StringEntity body = new StringEntity(gson.toJson(transferRequest));
        HttpPut request = new HttpPut("http://localhost:4567/transfer/");
        request.setEntity(body);

        // When
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String json = EntityUtils.toString(response.getEntity());
        Transfer transfer = gson.fromJson(json, Transfer.class);

        // Then
        assertNotNull(transfer);
        assertEquals(transfer.getResultSourceBalance(), -transferAmount);
        assertEquals(transfer.getResultTargetBalance(), transferAmount);
    }

}
