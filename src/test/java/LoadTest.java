import app.Application;
import app.account.Account;
import app.account.CreateAccountRequest;
import app.transfer.TransferRequest;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static spark.Spark.stop;

class LoadTest {

    Gson gson = new Gson();
    String fromName = "from";
    String toName = "to";
    int transferAmount = 10;

    @BeforeEach
    void startCleanServer() {
        Application.Run();
    }

    @AfterEach
    void stopServer() {
        stop();
    }

    private HttpUriRequest doCreateAccountRequest(String name) throws IOException {
        CreateAccountRequest createRequest = new CreateAccountRequest(name);
        StringEntity body = new StringEntity(gson.toJson(createRequest));
        HttpPut request = new HttpPut("http://localhost:4567/account/");
        request.setEntity(body);
        return request;
    }

    @Test
    void parallelLoadTest() throws IOException {

        // Given
        int parallelismDegree = 500;
        HttpClientBuilder.create().build().execute(doCreateAccountRequest(fromName));
        HttpClientBuilder.create().build().execute(doCreateAccountRequest(toName));

        TransferRequest transferRequest = new TransferRequest(fromName, toName, transferAmount);
        StringEntity body = new StringEntity(gson.toJson(transferRequest));

        // When
        ExecutorService executorService = Executors.newFixedThreadPool(parallelismDegree);
        for (int i = 0; i < parallelismDegree; i++) {
            executorService.execute(() -> {
                try {
                    HttpPut request = new HttpPut("http://localhost:4567/transfer/");
                    request.setEntity(body);
                    HttpClientBuilder.create().build().execute(request);
                } catch (IOException ignored) {

                }
            });
        }
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }

        // Then
        HttpUriRequest request = new HttpGet("http://localhost:4567/account/" + fromName);
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String json = EntityUtils.toString(response.getEntity());
        Account accountFrom = gson.fromJson(json, Account.class);

        request = new HttpGet("http://localhost:4567/account/" + toName);
        response = HttpClientBuilder.create().build().execute(request);
        json = EntityUtils.toString(response.getEntity());
        Account accountTo = gson.fromJson(json, Account.class);

        assertEquals(accountTo.getBalance(), transferAmount * parallelismDegree);
        assertEquals(accountFrom.getBalance(), - transferAmount * parallelismDegree);
    }
}
