import app.account.Account;
import app.account.AccountDAO;
import app.common.DaoException;
import app.common.EntityExistsException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void testCreation() throws DaoException {
        AccountDAO dao = new AccountDAO();
        dao.createAccount("aaa");
        Account aaa = dao.getAccount("aaa");
        assertNotNull(aaa);
    }

    @Test
    void testDuplicatedNames() throws EntityExistsException {
        AccountDAO dao = new AccountDAO();
        dao.createAccount("aaa");
        assertThrows(EntityExistsException.class,()-> dao.createAccount("aaa"));
    }



}
